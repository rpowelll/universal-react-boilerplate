# Universal React Boilerplate

[![license](https://img.shields.io/github/license/mashape/apistatus.svg?style=flat)](https://github.com/rpowelll/universal-react-app/blob/master/LICENSE)
[![Build
status](https://badge.buildkite.com/79b398bcf06fa502e4320c6732c8d40b936c423ca161f8aba2.svg)](https://buildkite.com/rhys-powell/universal-react-boilerplate)

_This repo provides a relatively full featured starting point for building
a modern, universal React application._

## Features

- Server-side rendering when running in production
- Hot module replacement when running in development
- Webpack for bundling assets
- Jest for running tests
- Redux and Redux Saga for managing application state
- A carefully chosen set of ESLint rules to enforce good coding style
- Flow type checking support
- Probably some more stuff
